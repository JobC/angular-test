import 'jquery/dist/jquery';
import angular from 'angular';
import uiRouter from 'angular-ui-router';
import Common from './common/common';
import Components from './components/components';
import AppComponent from './app.component';
import 'bootstrap/dist/js/bootstrap.js';

/* Datatables shit */
import 'datatables.net/js/jquery.dataTables.js';
import '../../bower_components/angular-datatables/demo/src/archives/dist/css/angular-datatables.css';
import datatables from '../../bower_components/angular-datatables/demo/src/archives/dist/angular-datatables';
import '../../bower_components/angular-datatables/demo/src/archives/dist/plugins/bootstrap/datatables.bootstrap.min.css';
import datatablesBootstrap from '../../bower_components/angular-datatables/demo/src/archives/dist/plugins/bootstrap/angular-datatables.bootstrap.js';

/* TOAST */
import 'angular-animate';
import 'ng-toast/dist/ngToast.css';
import 'ng-toast/dist/ngToast-animations.css';
import 'angular-sanitize/angular-sanitize.js';
import ngToast from 'ng-toast';

angular.module('app', [
    uiRouter,
    Common,
    Components,
    datatables,
    datatablesBootstrap,
    'ngToast'
])
    .config(($locationProvider, ngToastProvider) => {
        "ngInject";
        http:$locationProvider.html5Mode(true).hashPrefix('!');
        ngToastProvider.configure({
            animation: 'fade' // or 'fade'
        });
    })

    .component('app', AppComponent);
