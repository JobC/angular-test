export class Abstraction {
    constructor ($http, ngToast, DTOptionsBuilder, DTColumnBuilder) {
        'ngInject';
        this.$http = $http;
        this.$toast = ngToast;
        this.$DTOptionsBuilder = DTOptionsBuilder;
        this.$DTColumnBuilder = DTColumnBuilder;
    }

    toast (message, style) {
        let self = this;
        self.$toast.create({
            "className": style || 'info',
            "content": message
        });
    }

    get urlApi () {
        return "http://api.dev/";
    }

    get current () {
        return this._current();
    }

    set current (value) {
        this._current = value;
    }

    /*API METHODS*/

    save(url, data){
        let method = (url.id) ? 'put' : 'post';
        let request = this.$http[post](url, data);
        return request;
    }

    get(url){
        let request = this.$http.get(url);
        return request;
    }

    delete(url){
        let request = this.$http.delete(url);
        return request;
    }
}