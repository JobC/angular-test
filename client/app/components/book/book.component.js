import template from './book.html';
import controller from './book.controller';
import './book.scss';

let bookComponent = {
  bindings: {},
  template,
  controller
};

export default bookComponent;
