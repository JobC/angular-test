import angular from 'angular';
import uiRouter from 'angular-ui-router';
import bookComponent from './book.component';

let bookModule = angular.module('book', [
    uiRouter
])

    .config(($stateProvider) => {
        "ngInject";
        $stateProvider
            .state('book', {
                url: '/book',
                component: 'book'
            });
    })
    .component('book', bookComponent)

    .name;

export default bookModule;
