import BookModule from './book';
import BookController from './book.controller';
import BookComponent from './book.component';
import BookTemplate from './book.html';

describe('Book', () => {
  let $rootScope, makeController;

  beforeEach(window.module(BookModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new BookController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template [REMOVE]', () => {
      expect(BookTemplate).to.match(/{{\s?\$ctrl\.name\s?}}/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = BookComponent;

    it('includes the intended template', () => {
      expect(component.template).to.equal(BookTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(BookController);
    });
  });
});
