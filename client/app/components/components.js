import angular from 'angular';
import Home from './home/home';
import About from './about/about';
import Book from './book/book';

let componentModule = angular.module('app.components', [
    Home,
    About,
    Book

])

    .name;

export default componentModule;
