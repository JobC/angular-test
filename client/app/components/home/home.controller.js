import {Abstraction} from "../abstraction"
export class HomeController extends Abstraction {
    $onInit () {
        let self = this;
        this.title = 'books';
        this.showTable = false;
        // self.reloadlist();
    }

    reloadlist () {
        let self = this;
        self.showTable = false;
        let books = [];
        self.get(self.urlApi + 'books').then((response) => {
            if (response.status == 200) {
                books = JSON.stringify(response.data.data);
                self.dtOptions = self.$DTOptionsBuilder.fromSource(books).withPaginationType('full_numbers');
                self.dtColumns = [
                    self.$DTColumnBuilder.newColumn('id').withTitle('ID'),
                    self.$DTColumnBuilder.newColumn('name').withTitle('First name'),
                    self.$DTColumnBuilder.newColumn('author').withTitle('author'),
                    self.$DTColumnBuilder.newColumn('categories_id').withTitle('categories_id'),
                    self.$DTColumnBuilder.newColumn('published_at').withTitle('published_at'),
                    self.$DTColumnBuilder.newColumn('users_id').withTitle('users_id')
                ];
                self.showTable = true;
            } else {
                self.toast('Error to load books :(', 'danger');
            }
        }, () => {
            self.toast('Error to load books :(', 'danger');
        });
    }
}

export default HomeController;
